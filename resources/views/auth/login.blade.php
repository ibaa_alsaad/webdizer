@extends('layouts.login')
@section('title','الدخول')
@section('content')
    <!-- Background -->
    <div class="account-pages"></div>
    <!-- Begin page -->
    <div class="wrapper-page">

        <div class="card" style="text-align: right ; direction: rtl;">
            <div class="card-body">

                <h3 class="text-center m-0">
                    <a href="index.html" class="logo logo-admin">
                        <img src="{{asset('/assets/images/THE WEBDIZER LOGO-03.png')}}" width="45%" alt="logo">
                    </a>
                </h3>

                <div class="px-3">
                    <h3 class="text-muted font-18 m-b-5 text-center"> تسجيل الدخول </h3>

                    <form class="form-horizontal m-t-30" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email">البريد الإلكتروني</label>
                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">كلمة المرور</label>
                            <input type="password" id="password" placeholder="" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group row m-t-20">
                            <div class="col-6 text-right">
                                <button class="btn btn-primary w-md waves-effect waves-light" type="submit">تسجيل الدخول </button>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-link" href="{{ route('Password') }}">
                                    هل نسيت كلمة المرور؟
                                </a>
{{--                                <div class="custom-control custom-checkbox">--}}
{{--                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="custom-control-label" for="remember">--}}
{{--                                        تذكرني--}}
{{--                                    </label>--}}
{{--                                </div>--}}
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>

    <!-- END wrapper -->

@endsection
