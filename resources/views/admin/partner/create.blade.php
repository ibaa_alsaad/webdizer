﻿@extends('layouts.admin')
@section('title')
    | إضافة شخص جديد
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">لوحة التحكم </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{url('Admin/partners')}}"> الشركاء </a>
                                </li>
                                <li class="breadcrumb-item active">إضافة شخص جديد
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                   @include('admin.includes.alerts.success')
                                   @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{url('Admin/partners')}}" method="POST"
                                              enctype="multipart/form-data">
                                              @csrf
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> بيانات الشخص </h4>

                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="link">رابط الصفحة الشخصية</label>
                                                            <input type="text"
                                                                   value="{{ old('link') }}"
                                                                   required
                                                                   id="link"
                                                                   class="form-control"
                                                                   placeholder="ادخل الرابط "
                                                                   name="link">
                                                            @error("link")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label> صورة الشخص  </label>
                                                            <label id="image" class="file center-block">
                                                                <input type="file" id="image" name="image">
                                                                <span class="file-custom"></span>
                                                            </label>
                                                            @error("image")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="form-actions">
                                                <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                        onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>
                                                <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                    <i class="la la-check-square-o"></i> حفظ
                                                </button>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
