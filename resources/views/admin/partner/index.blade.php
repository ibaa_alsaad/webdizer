﻿@extends('layouts.admin')
@section('title')
    | الشركاء
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('admin\dashboard')}}">لوحة التحكم</a>
                                </li>
                                <li class="breadcrumb-item active"> قائمة الشركاء
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- DOM - jQuery events table -->
                <section id="dom">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard" style="text-align: center">
                                        <table
                                            class="table display nowrap table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">رابط الصفحة الشخصية</th>
                                                <th scope="col"> الصورة </th>
                                                <th scope="col">العمليات</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                    @isset($partners)
                                        @foreach ($partners as $key => $partner)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td style="width: 114px">
                                                    <a target="_blank" href="{{ $partner->link }}">{{ $partner->link }}</a>
                                                </td>

                                                <td><img width="150px" height="100px"  src="{{$partner->image}}"></td>

                                                <td>
{{--                                                    @can(' تعديل Slider')--}}
                                                        <a href="{{ route('partners.edit', $partner->id) }}" class="btn btn-sm btn-outline-info box-shadow-1"
                                                           title="تعديل"><i class="la la-pencil-square"></i>تعديل </a>
{{--                                                    @endcan--}}

{{--                                                    @can(' حذف Slider')--}}

                                                    <button class=" btn btn-sm btn-outline-danger box-shadow-1" data-delid={{$partner->id}} data-toggle="modal" data-target="#delete"
                                                            title="حذف" >
                                                        <i class="la la-trash"></i>حذف </button>
{{--                                                    @endcan--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset

                                            </tbody>
                                        </table>
                                        {{$partners->links('admin.pagination')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @isset($partner)
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                    </div>

                                    <!-- Modal -->
                                    <form action="{{url("Admin/partners/{$partner->id}")}}" method="post">
                                        {{method_field('DELETE')}}
                                        {{csrf_field()}}
                                        <div class="modal-body">
                                            <h4>   هل أنت متأكد من حذف هذا العنصر؟</h4>
                                            <input type="hidden" name="delet_id" id="del_id" value="">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">إلغاء</button>
                                            <button type="submit" class="btn btn-warning">موافق</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endisset
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('admin/js/scripts/delete.js')}}" type="text/javascript"></script>
@endsection
