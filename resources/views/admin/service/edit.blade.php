﻿@extends('layouts.admin')
@section('title')
    | تعديل خدمة
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{url('Admin/service')}}"> الخدمات  </a>
                                </li>
                                <li class="breadcrumb-item active"> تعديل الخدمة
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{route('service.update',$service->id)}}" method="POST"
                                              enctype="multipart/form-data">
                                            @method('PATCH')
                                            @csrf

                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> بيانات  الخدمة </h4>

                                                   <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1"> اسم الخدمة AR</label>
                                                                    <input type="text"
                                                                           value="{{$service->{'title:ar'} }}"
                                                                           id="title"
                                                                           class="form-control"
                                                                           placeholder="ادخل اسم القسم  "
                                                                           name="title:ar">
                                                                    @error("title")
                                                                    <span class="text-danger">{{$message}} </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                       <div class="col-md-6">
                                                           <div class="form-group">
                                                               <label for="projectinput1"> اسم الخدمة EN</label>
                                                               <input type="text"
                                                                      value="{{$service->{'title:ar'} }}"
                                                                      id="title"
                                                                      class="form-control"
                                                                      placeholder="ادخل اسم القسم  "
                                                                      name="title:en">
                                                               @error("title")
                                                               <span class="text-danger">{{$message}} </span>
                                                               @enderror
                                                           </div>
                                                       </div>
                                                    </div>

                                                   <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                    <img src="{{asset("/admin/{$service->image}")}}"
                                                                         class=" height-150" alt="صورة الخدمة">
                                                            </div>
                                                            <div class="form-group">
                                                                <label> صوره الخدمة </label>
                                                                <label id="image" class="file center-block">
                                                                    <input type="file" id="image" name="image">
                                                                    <span class="file-custom"></span>
                                                                </label>
                                                                @error("image")
                                                                <span class="text-danger">{{$message}} </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text">AR الوصف </label>
                                                            <textarea type="text"
                                                                      id="text"
                                                                      rows="5"
                                                                      class="form-control"
                                                                      placeholder="ادخل الوصف "
                                                                      name="text:ar">
                                                                {{$service->{'text:ar'} }}
                                                                @error("text")
                                                                <span class="text-danger">{{$message}} </span>
                                                                @enderror
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> الوصف EN</label>
                                                            <textarea type="text"
                                                                      id="text"
                                                                      rows="5"
                                                                      class="form-control"
                                                                      placeholder="ادخل الوصف "
                                                                      name="text:en">
                                                                {{$service->{'text:en'} }}
                                                                @error("text")
                                                                <span class="text-danger">{{$message}} </span>
                                                                @enderror
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                   <div class="form-actions">
                                                <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                        onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>
                                                <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                    <i class="la la-check-square-o"></i> حفظ
                                                </button>
                                            </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

@endsection
