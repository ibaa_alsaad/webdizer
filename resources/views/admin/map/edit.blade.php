@extends('layouts.admin')
@section('title')
    | حول الشركة | تعديل
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item active">تعديل الوصف
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form id="myForm" class="form" action="{{url('Admin/editAbout')}}" method="POST"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="about_ar"> الوصف AR </label>
                                                            <textarea type="text"
                                                                      id="about_ar"
                                                                      rows="10"
                                                                      class="form-control note-editable"
                                                                      placeholder="ادخل الوصف "
                                                                      name="about:ar"
                                                                      value="{{ $about_ar->{'value:ar'} }}"
                                                                      style="direction: ltr"
                                                                      required >
                                                                {{$about_ar->{'value:ar'} }}
                                                                @error("about_ar")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="about_en"> الوصف EN </label>
                                                            <textarea type="text"
                                                                      id="about_en"
                                                                      rows="10"
                                                                      class="form-control note-editable"
                                                                      placeholder="ادخل الوصف "
                                                                      name="about:en"
                                                                      value="{{ $about_en->{'value:en'}  }}"
                                                                      style="direction: ltr"
                                                                      required >
                                                                {{$about_en->{'value:en'} }}
                                                            @error("about_en")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                            onclick="history.back();">
                                                        <i class="ft-x"></i> تراجع
                                                    </button>
                                                    @can('تعديل حول الشركة')
                                                    <button  type="submit" class="btn btn-outline-primary box-shadow-1">
                                                        <i class="la la-check-square-o"></i> تعديل
                                                    </button>
                                                    @endcan
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
