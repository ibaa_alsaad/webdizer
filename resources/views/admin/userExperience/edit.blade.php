﻿@extends('layouts.admin')
@section('title')
    | تجارب المستخدمين | تعديل
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">لوحة التحكم </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{url('Admin/service')}}"> تجارب المستخدمين  </a>
                                </li>
                                <li class="breadcrumb-item active"> تعديل البيانات
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{route('userExperience.update',$userExperiences->id)}}" method="POST"
                                              enctype="multipart/form-data">
                                            @method('PATCH')
                                            @csrf

                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> بيانات التجربة </h4>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name"> الاسم AR</label>
                                                            <input type="text"
                                                                   value="{{ $userExperiences->{'name:ar'} }}"
                                                                   id="name"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم الشخص "
                                                                   name="name:ar">
                                                            @error("name")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name"> الاسم EN </label>
                                                            <input type="text"
                                                                   value="{{ $userExperiences->{'name:en'} }}"
                                                                   id="name"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم الشخص"
                                                                   name="name:en">
                                                            @error("name")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> التجربة AR</label>
                                                            <textarea type="text"
                                                                      id="text"
                                                                      rows="5"
                                                                      class="form-control"
                                                                      placeholder="ادخل النص "
                                                                      name="text:ar">
                                                                {{ $userExperiences->{'text:ar'} }}
                                                            @error("text")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> التجربة EN </label>
                                                            <textarea type="text"
                                                                      id="text"
                                                                      rows="5"
                                                                      class="form-control"
                                                                      placeholder="ادخل النص "
                                                                      name="text:en">
                                                                {{ $userExperiences->{'text:en'} }}
                                                            @error("text")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions">
                                                <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                        onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>
                                                <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                    <i class="la la-check-square-o"></i> حفظ
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

@endsection
