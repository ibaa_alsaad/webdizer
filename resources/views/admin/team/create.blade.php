﻿@extends('layouts.admin')
@section('title')
    | إضافة شخص جديد
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{url('Admin/team')}}"> الفريق </a>
                                </li>
                                <li class="breadcrumb-item active">إضافة شخص جديد
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                   @include('admin.includes.alerts.success')
                                   @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{url('Admin/team')}}" method="POST"
                                              enctype="multipart/form-data">
                                              @csrf

                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> البيانات الشخصية </h4>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name"> الاسم AR</label>
                                                            <input type="text"
                                                                   value="{{ old('name') }}"
                                                                   required autocomplete="name"
                                                                   id="name"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم الشخص "
                                                                   name="name:ar">
                                                            @error("name:ar")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name"> الاسم EN </label>
                                                            <input type="text"
                                                                   value="{{ old('name') }}"
                                                                   required autocomplete="name"
                                                                   id="name"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم الشخص"
                                                                   name="name:en">
                                                            @error("name:en")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="job"> المسمى الوظيفي AR</label>
                                                            <input type="text"
                                                                   value="{{ old('job') }}"
                                                                   id="job"
                                                                   class="form-control"
                                                                   placeholder="ادخل الاسم هنا "
                                                                   name="job:ar">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="job"> المسمى الوظيفي EN </label>
                                                            <input type="text"
                                                                   value="{{ old('job') }}"
                                                                   id="job"
                                                                   class="form-control"
                                                                   placeholder="ادخل الاسم هنا"
                                                                   name="job:en">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label> الصورة  </label>
                                                            <label id="image" class="file center-block">
                                                                <input type="file" id="image" name="image">
                                                                <span class="file-custom"></span>
                                                            </label>
                                                            @error("image")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> الوصف AR</label>
                                                            <textarea type="text"
                                                                   value="{{ old('text') }}"
                                                                   id="text"
                                                                   rows="5"
                                                                   class="form-control"
                                                                   placeholder="ادخل الوصف "
                                                                   name="text:ar"
                                                                   required >
                                                            </textarea>
                                                            @error("text:ar")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> الوصف EN </label>
                                                            <textarea type="text"
                                                                      value="{{ old('text') }}"
                                                                      id="text"
                                                                      rows="5"
                                                                      class="form-control"
                                                                      placeholder="ادخل الوصف "
                                                                      name="text:en"
                                                                      required >
                                                            </textarea>
                                                            @error("text:en")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="form-section"><i class="ft-home"></i> روابط التواصل الاجتماعي </h4>
                                                <div class="row">
                                                    <div class="col-md-6" >
                                                        <div class="form-group ">
                                                            <label for="facebook"> Facebook </label>
                                                            <input type="text"
                                                                   value="{{ old('facebook') }}"
                                                                   id="facebook"
                                                                   class="form-control"
                                                                   placeholder="ادخل الرابط هنا  "
                                                                   name="facebook">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" >
                                                        <div class="form-group ">
                                                            <label for="twitter"> twitter </label>
                                                            <input type="text"
                                                                   value="{{ old('twitter') }}"
                                                                   id="twitter"
                                                                   class="form-control"
                                                                   placeholder="ادخل الرابط هنا  "
                                                                   name="twitter">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6" >
                                                        <div class="form-group ">
                                                            <label for="linkedin"> linkedin </label>
                                                            <input type="text"
                                                                   value="{{ old('linkedin') }}"
                                                                   id="linkedin"
                                                                   class="form-control"
                                                                   placeholder="ادخل الرابط هنا  "
                                                                   name="linkedin" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" >
                                                        <div class="form-group ">
                                                            <label for="youtube"> youtube </label>
                                                            <input type="text"
                                                                   value="{{ old('youtube') }}"
                                                                   id="youtube"
                                                                   class="form-control"
                                                                   placeholder="ادخل الرابط هنا  "
                                                                   name="youtube">
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-actions">
                                                <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                        onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>
                                                <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                    <i class="la la-check-square-o"></i> حفظ
                                                </button>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
