﻿@extends('layouts.admin')
@section('title')
    | إضافة مستحدم جديد
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{url('Admin/users')}}"> المستخدمين </a>
                                </li>
                                <li class="breadcrumb-item active">إضافة مستخدم جديد
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                   @include('admin.includes.alerts.success')
                                   @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{url('Admin/users')}}" method="POST"
                                              enctype="multipart/form-data">
                                              @csrf

                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> بيانات المستخدم </h4>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name"> الاسم</label>
                                                            <input type="text"
                                                                   value="{{ old('name') }}"
                                                                   required autocomplete="name"
                                                                   id="name"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم المستخدم  "
                                                                   name="name">
                                                            @error("name")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6" >
                                                        <div class="form-group">
                                                            <label for="email"> البريد الإلكتروني </label>
                                                            <input type="email"
                                                                   value="{{ old('email') }}"
                                                                   id="email"
                                                                   class="form-control"
                                                                   placeholder="ادخل البريد الإلكتروني "
                                                                   name="email"
                                                                   required autocomplete="email">
                                                            @error("email")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="password"> كلمة المرور</label>
                                                            <input type="password"
                                                                   value="{{ old('password') }}"
                                                                   id="password"
                                                                   class="form-control"
                                                                   placeholder="ادخل كلمة المرور"
                                                                   required name="password">
                                                            @error("password")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6" >
                                                        <div class="form-group">
                                                            <label for="role_name"> نوع المستخدم </label>
                                                            <select class="form-control" name="role_name" id="role_name">
                                                                @foreach($roles as $role)
                                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error("role_name")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group mt-1">
                                                            <input type="checkbox" value="1" name="status"
                                                                   id="status"
                                                                   class="switchery" data-color="success"
                                                                   checked/>
                                                            <label for="status"
                                                                   class="card-title ml-1">حالة المستخدم </label>

                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="form-actions">
                                                <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                        onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>
                                                <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                    <i class="la la-check-square-o"></i> حفظ
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
