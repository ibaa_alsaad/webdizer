<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('title')</title>

    <meta name="keywords" content="THE WEBDIZER " />
    <meta name="description" content="THE WEBDIZER ">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('front/img/THE WEBDIZER LOGO2.png')}}" type="image/x-icon" />

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">



    <!-- Web Fonts  -->
    <link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light&display=swap" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->

    <link rel="stylesheet" href="{{asset('front/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/vendor/animate/animate.compat.css')}}">
    <link rel="stylesheet" href="{{asset('front/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/vendor/magnific-popup/magnific-popup.min.css')}}">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('front/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/theme-elements.css')}}">
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{asset('front/vendor/circle-flip-slideshow/css/component.css')}}">

    <!-- Skin CSS -->
    <link id="skinCSS" rel="stylesheet" href="{{asset('front/css/skins/default.css')}}">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">
    @if(LaravelLocalization::getCurrentLocale()=='ar')
        <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">
        <link href="{{asset('front/css/style-ar.css')}}" rel="stylesheet">
        @yield('style')
@endif
<!-- Head Libs -->
{{--    <script src="{{asset('front/vendor/modernizr/modernizr.min.js')}}"></script>--}}

</head>
<body class="one-page alternative-font-5">
<div class="body">

    <!-- Begin Header -->
@include('front.includes.header')
<!--End  Header -->


@yield('content')

<!-- Begin Footer -->
@include('front.includes.footer')
<!-- End Footer -->

</div>
<!-- Vendor -->
<script src="{{asset('front/vendor/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('front/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
<script src="{{asset('front/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('front/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
<script src="{{asset('front/vendor/popper/umd/popper.min.js')}}"></script>
<script src="{{asset('front/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('front/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('front/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('front/vendor/isotope/jquery.isotope.min.js')}}"></script>
<script src="{{asset('front/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('front/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js" integrity="sha512-dDguQu7KUV0H745sT2li8mTXz2K8mh3mkySZHb1Ukgee3cNqWdCFMFsDjYo9vRdFRzwBmny9RrylAkAmZf0ZtQ==" crossorigin="anonymous"></script>

<!-- Current Page Vendor and Views -->
<script src="{{asset('front/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js')}}"></script>
<script src="{{asset('front/js/views/view.home.js')}}"></script>
<!-- Theme Base, Components and Settings -->
<script src="{{asset('front/js/theme.js')}}"></script>

<!-- Theme Initialization Files -->
<script src="{{asset('front/js/theme.init.js')}}"></script>

<!-- Examples -->
<script src="{{asset('front/js/examples/examples.portfolio.js')}}"></script>
<script src="{{asset('front/js/views/view.contact.js')}}"></script>

@yield('script')
{{--    <script type="text/javascript">--}}
{{--        $(function(){--}}
{{--            $('.nav a').filter(function(){return this.href==location.href}).addClass('active').siblings().removeClass('active')--}}
{{--            $('.nav a').click(function(){--}}
{{--                $(this).addClass('active').siblings().removeClass('active')--}}
{{--            })--}}
{{--        })--}}
{{--</script>--}}
{{--<script type="text/javascript">--}}
{{--    jQuery(document).ready(function(){--}}
{{--        jQuery('.nav a').filter(function(){return this.href==location.href}).parent().addClass('active').siblings().removeClass('active')--}}
{{--        jQuery('.nav a').click(function(){--}}
{{--            jQuery(this).parent().addClass('active').siblings().removeClass('active')--}}
{{--        })--}}
{{--    })--}}
{{--</script>--}}
{{--<script>--}}
{{--    $(document).ready(function(){--}}
{{--        $('ul li a').click(function(){--}}
{{--            $('li a').removeClass("active");--}}
{{--            $(this).addClass("active");--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}
<script>
    $('#header ul li a').click(function() {
        $('li a').removeClass("active");
        $(this).addClass("active");
        localStorage.setItem('active', $(this).parent().index());
    });

    var ele = localStorage.getItem('active');
    $('#header ul li:eq(' + ele + ')').find('a').addClass('active');

</script>
