@extends('layouts.site')
@section('title')
    {{__('app.main_page')}}
@endsection
@section('content')
<div class="main">
    <section class=" page-header-modern page-header-modern bg-color-primary page-header-md m-0">
        <div class="container">
            <div class="row p-5">

                <div class="col-md-12 align-self-center p-static order-2 text-center">


                    <h1 class="text-light text-10"><strong>{{__('app.Search')}}</strong></h1>
                    <span class="sub-title text-light">
                        @if ($searchResults-> isEmpty())
                            <span class="sub-title pt-2">"{{__('app.Sorry, no results found for the term')}} <b>({{ $searchterm }})"</b></span>
                        @else
                            <span class="sub-title pt-2">{{__('app.There are')}} {{ $searchResults->count() }} {{__('app.results for the term ')}} <strong>"{{ $searchterm }}" </strong></span>
                        @endif
                    </span>
                </div>

            </div>
        </div>
    </section>
    <hr class="m-0">
    <div class="container py-5 mt-3" @if(LaravelLocalization::getCurrentLocale()=='en') dir="ltr" @else dir="rtl" style="text-align: right" @endif>
        <div class="row">
            <div class="col">
                <h2 class="font-weight-normal text-7 mb-0">{{__('app.Showing results for')}} <strong class="font-weight-extra-bold">{{ $searchterm }}</strong></h2>
                <p class="lead mb-0">{{__('app.results found.')}} {{ $searchResults->count() }} {{__('app.results')}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col pt-2 mt-1">
                <hr class="my-4">
            </div>
        </div>
        <div class="row">
            <div class="col">
                @if(isset($searchResults))
                        @foreach($searchResults->groupByType() as $type => $modelSearchResults)
                            @foreach($modelSearchResults as $key => $searchResult)
                                <ul>
                                    <li>
                                        <div class="post-info">
                                            <a href="{{ $searchResult->url }}">{{ $searchResult->title }}</a>
                                            <div class="post-meta">
                                            <span class="text-dark text-uppercase font-weight-semibold"> {!!  \Illuminate\Support\Str::limit( $searchResults[++$key]->searchable->text, 150) !!}</span> | {{ $searchResults[++$key]->searchable->created_at }}
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            @endforeach
                        @endforeach
                @endif
            </div>
{{--            {{$searchResults->render()}}--}}
        </div>
    </div>
    <section class="section section-default border-0 m-0">
        <div class="container py-4">
            <div class="row pb-4">
                <div class="col">
                    <form role="search"  action="{{ route('search') }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group input-group-lg">
                            <input class="form-control h-auto" placeholder="Search..."  name="query" value="{{ isset($searchterm) ? $searchterm : ''  }}" id="s" type="text">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
