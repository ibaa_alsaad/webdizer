@extends('layouts.site')

@section('title')
    {{__('app.services')}}
@endsection
@section('content')
    <div role="main" class="main" @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right" @else dir="ltr" @endif>
        <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{asset('front/img/img-services1.jpg')}}); background-position-y: center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <h1 class="text-10">{{__('app.services')}}</h1>
                        <span class="sub-title pt-2">{{__('app.we are here to help you')}}</span>
                    </div>
                </div>
            </div>
        </section>
       <div class="container-fluid">
           @foreach($services as $service)
               @if($service->class=='left')
        <div class="row align-items-center bg-color-grey">
            <div class="col-lg-6 p-0">
                <section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'Position': '100%'}" @if($service->image != null) data-image-src="{{asset("admin/{$service->image}")}}" @else data-image-src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif style="min-height: 600px; background: #778cbf;">
                </section>
            </div>
            <div class="col-lg-6 p-0">
                <section class="section section-no-border h-100 m-0">
                    <div class="row justify-content-center m-0">
                        <div class="col-half-section col-half-section-left" style="margin: 0 30px;">
                            <div class="overflow-hidden">
                                <h4 class="mb-0 appear-animation" data-appear-animation="maskUp"><a href="#" class="text-4 font-weight-bold pt-2 d-block text-dark text-decoration-none pb-1">{{$service->title}}</a></h4>
                            </div>

                            <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{$service->text}}</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
               @else
        <div class="row align-items-center bg-color-grey">
        <div class="col-lg-6 order-2 order-lg-1 p-0">
            <section class="section section-no-border h-100 m-0">
                <div class="row justify-content-center m-0">
                    <div class="col-half-section col-half-section-right custom-text-align-right">
                        <div class="overflow-hidden">
                            <h4 class="mb-0 appear-animation" data-appear-animation="maskUp"><a href="#" class="text-4 font-weight-bold pt-2 d-block text-dark text-decoration-none pb-1">{{$service->title}}</a></h4>
                        </div>

                        <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">{{$service->text}}</p>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 p-0">
            <section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'Position': '100%'}" @if($service->image != null) data-image-src="{{asset("admin/{$service->image}")}}" @else data-image-src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif  style="min-height: 600px ;background: #778cbf;">
            </section>
        </div>
    </div>
               @endif
           @endforeach
</div>
    </div>
@endsection
