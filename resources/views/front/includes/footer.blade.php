<?php
$variable =\App\Models\Variable::all();
if(\LaravelLocalization::getCurrentLocale()=='ar')
    $about=App\Models\Variable::where('name','about_ar')->first();
else
    $about=App\Models\Variable::where('name','about_en')->first();
?>
<footer id="footer"  @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right;
" @else dir="ltr" @endif>
    <div class="container">
        <div class="footer-ribbon">
            <span>{{__('app.contact')}}</span>
        </div>
        <div class="row py-5 my-4">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <img src="{{asset('front/img/THE_WEBDIZER_LOGO-04.png')}}" style="width: 100%;top: -104px;position: absolute;left: -104px;">
            </div>

            <div class="col-md-6 col-lg-2 mb-4 mb-md-0">
                <div class="contact-details">
                    <h5 class="text-3 mb-3">{{__('app.pages')}}</h5>
                    <ul class="list list-icons list-icons-lg">
                        <li class="mb-1 pl-1">
                            <p class="m-0"><a href="{{url('project')}}">{{__('app.project')}}</a></p></li>
                        <li class="mb-1 pl-1">
                            <p class="m-0"><a href="{{url('service')}}">{{__('app.services')}}</a></p></li>
                        <li class="mb-1 pl-1">
                            <p class="m-0"><a href="{{url('news')}}">{{__('app.News')}}</a></p></li>
                        <li class="mb-1 pl-1">
                            <p class="m-0"><a href="{{url('team')}}"> {{__('app.Team')}}</a></p></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                <div class="contact-details">
                    <h5 class="text-3 mb-3">{{__('app.contact')}}</h5>
                    <ul class="list list-icons list-icons-lg" @if(LaravelLocalization::getCurrentLocale()=='ar') style="text-align: center" @endif>

                        @if(LaravelLocalization::getCurrentLocale()=='ar')
                              @if($variable[0]->getValueADR('address','ar') != null)
                                <li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i>
                                   <p class="m-0">{{$variable[0]->getValueADR('address','ar')}}</p>
                                </li>
                              @endif
                        @else
                            @if($variable[0]->getValueADR('address','en') != null)
                                <li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i>
                                    <p class="m-0">{{$variable[0]->getValueADR('address','en')}}</p>
                                </li>
                            @endif
                        @endif
                        @if($variable[0]->getValue('whatsup') != null)
                        <li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i>
                            <p class="m-0"><a target="_blank" href="tel:{{$variable[0]->getValue('whatsup')}}">{{$variable[0]->getValue('whatsup')}}</a></p>
                        </li>
                        @endif
                        @if($variable[0]->getValue('email') != null)
                        <li class="mb-1"><i class="far fa-envelope text-color-primary"></i>
                            <p class="m-0"><a  href="mailto:{{$variable[0]->getValue('email')}}">{{$variable[0]->getValue('email')}}</a></p></li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <h5 class="text-3 mb-3">{{__('app.about')}}</h5>
                <p class="pr-1"> {!!  \Illuminate\Support\Str::limit( $about->value, 200) !!}</p>

                <h5 class="text-3 mb-3">{{__('app.Social Media')}}</h5>
                <ul class="social-icons">
                    @if($variable[0]->getValue('facebook') != null)
                    <li class="social-icons-facebook"><a href="{{$variable[0]->getValue('facebook')}}" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('twitter') != null)
                    <li class="social-icons-twitter"><a href="{{$variable[0]->getValue('twitter')}}" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('linkedin') != null)
                    <li class="social-icons-linkedin"><a href="{{$variable[0]->getValue('linkedin')}}" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('whatsup') != null)
                        <li class="social-icons-whatsapp"><a href="{{$variable[0]->getValue('whatsup')}}" target="_blank" title="whatsapp"><i class="fab fa-whatsapp"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('instagram') != null)
                        <li class="social-icons-instagram"><a href="{{$variable[0]->getValue('instagram')}}" target="_blank" title="instagram"><i class="fab fa-instagram"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('youtube') != null)
                        <li class="social-icons-youtube"><a href="{{$variable[0]->getValue('youtube')}}" target="_blank" title="youtube"><i class="fab fa-youtube"></i></a></li>
                    @endif
                </ul>
            </div>



        </div>

    </div>
    <div class="footer-copyright">
        <div class="container py-2">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-center justify-content-center mb-4 mb-lg-0">
                    <p>Copyright &copy; {{date('Y')}}</p>
                </div>
            </div>
        </div>
    </div>
</footer>
