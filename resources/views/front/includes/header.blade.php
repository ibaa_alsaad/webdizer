<?php
$projects=\App\Models\Project::orderby('id','desc')->get();
$news=\App\Models\News::where('status',1)->orderby('id','desc')->get();
$categories1 = \App\Models\Category::with('Project')->where('type', '1')->get();
$categories2 = \App\Models\Category::with('News')->where('type', '2')->get();
$variable =\App\Models\Variable::all();
?>
<header id="header" class="header-effect-shrink" @if(LaravelLocalization::getCurrentLocale()=='en') dir="ltr" @else dir="rtl" @endif>
<div class="top-header btn-dark">
    <div class="container">
        <div class="row">
            <div id="list-icons" class="col-md-6 col-lg-6 mb-4 mb-md-0" @if(LaravelLocalization::getCurrentLocale()=='en') style="text-align: left;" @else style="text-align: right;" @endif>
                <div class="header-nav-features" style=" margin: auto 0;padding: 0 10px;">
                    <div class="header-nav-feature header-nav-features-search d-inline-flex">
                        <a id="headerSearch" href="#" class="header-nav-features-toggle text-decoration-none" data-focus="headerSearch">
                            <i class="fas fa-search header-nav-top-icon" style="color: #fff"></i></a>
                        <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed" id="headerTopSearchDropdown">
                            <form role="search"  action="{{ route('search') }}"  method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="simple-search input-group" style="direction: initial;">
                                    <input class="form-control text-1" id="headerSearch" name="query" type="search" value="" placeholder="Search...">
                                    <span class="input-group-append">
                                        <button class="btn" type="submit">
                                            <i class="fas fa-search header-nav-top-icon"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <ul class="list list-icons" style="margin: auto 10px; display: inline-flex">
                    @if($variable[0]->getValue('phone') != null)
                        <li class="mb-1" style="padding: 5px 27px 0;"><i class="fas fa-phone"style="color: #fff;padding-top: 8px;"></i>
                            <p class="m-0">
                                <a target="_blank" href="tel:{{$variable[0]->getValue('phone')}}"style="color: #fff;">{{$variable[0]->getValue('phone')}}</a></p>
                        </li>
                    @endif
                    @if($variable[0]->getValue('email') != null)
                        <li class="mb-1" style="padding: 5px 27px 0;"><i class="far fa-envelope" style="color: #fff;padding-top: 8px;"></i>
                            <p class="m-0">
                                <a  href="mailto:{{$variable[0]->getValue('email')}}"style="color: #fff;">{{$variable[0]->getValue('email')}}</a></p></li>
                    @endif
                </ul>

            </div>

            <div id="socl-icons" class="col-md-6 col-lg-6 mb-4 mb-md-0" @if(LaravelLocalization::getCurrentLocale()=='en') style="text-align: right;" @else style="text-align: left;" @endif>
                <ul class="social-icons" style="padding: 5px 27px;">
                    @if($variable[0]->getValue('facebook') != null)
                        <li class="social-icons-facebook"><a href="{{$variable[0]->getValue('facebook')}}" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('twitter') != null)
                        <li class="social-icons-twitter"><a href="{{$variable[0]->getValue('twitter')}}" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('linkedin') != null)
                        <li class="social-icons-linkedin"><a href="{{$variable[0]->getValue('linkedin')}}" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('whatsup') != null)
                        <li class="social-icons-whatsapp"><a href="{{$variable[0]->getValue('whatsup')}}" target="_blank" title="whatsapp"><i class="fab fa-whatsapp"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('instagram') != null)
                        <li class="social-icons-instagram"><a href="{{$variable[0]->getValue('instagram')}}" target="_blank" title="instagram"><i class="fab fa-instagram"></i></a></li>
                    @endif
                    @if($variable[0]->getValue('youtube') != null)
                        <li class="social-icons-youtube"><a href="{{$variable[0]->getValue('youtube')}}" target="_blank" title="youtube"><i class="fab fa-youtube"></i></a></li>
                    @endif
                </ul>
            </div>

        </div>
    </div>
</div>
    <div class="header-body" style="position: static">
        <div class="header-container container" style="height: 80px;">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="#ds">
                                <img alt="THE WEBDIZER LOGO" width="180" height="100" data-sticky-width="170" data-sticky-height="120" src="{{asset('front/img/THE WEBDIZER LOGO-02.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills links" id="mainNav">
                                        <li class="dropdown  ">
                                        <a class="dropdown-item dropdown-toggle active" href="{{url('/')}}">
                                                {{__('app.main')}}
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="{{url('about')}}">
                                                {{__('app.about')}}
                                            </a>
                                        </li>
                                        <li class="dropdown dropdown-mega">
                                            <a class="dropdown-item dropdown-toggle" href="{{url('project')}}">
                                                {{__('app.project')}}
                                            </a>
{{--                                            <ul class="dropdown-menu">--}}
{{--                                                <li>--}}
{{--                                                    <div class="dropdown-mega-content">--}}
{{--                                                        <div class="row  text-center">--}}
{{--                                                                @foreach($categories1 as $category)--}}
{{--                                                                @if (count($category->Project))--}}
{{--                                                                    <div class="col-lg-2 pt-3">--}}
{{--                                                                        <span class="dropdown-mega-sub-title">{{$category->name}}</span>--}}
{{--                                                                        <ul class="dropdown-mega-sub-nav">--}}
{{--                                                                            @foreach($projects as $project)--}}
{{--                                                                                @if($project ->CategoryID == $category->id)--}}
{{--                                                                            <li>--}}
{{--                                                                                <a class="dropdown-item" href="{{url("project/$project->id")}}" data-ajax-on-modal>--}}
{{--                                                                                    @if(LaravelLocalization::getCurrentLocale()=='en'){{$project->{'title:en'} }}--}}
{{--                                                                                    @else{{$project->{'title:ar'} }}@endif--}}
{{--                                                                                </a>--}}
{{--                                                                            </li>--}}
{{--                                                                                @endif--}}
{{--                                                                            @endforeach--}}
{{--                                                                        </ul>--}}
{{--                                                                    </div>--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="{{url('service')}}">
                                                {{__('app.services')}}
                                            </a>
                                        </li>
                                        <li class="dropdown dropdown-mega">
                                            <a class="dropdown-item dropdown-toggle" href="{{url('news')}}">
                                                {{__('app.News')}}
                                            </a>
{{--                                            <ul class="dropdown-menu">--}}
{{--                                                <li>--}}
{{--                                                    <div class="dropdown-mega-content">--}}
{{--                                                        <div class="row  text-center">--}}
{{--                                                            @foreach($categories2 as $category)--}}
{{--                                                                @if ($category->News)--}}
{{--                                                                        <div class="col-lg-2 pt-3">--}}
{{--                                                                            <span class="dropdown-mega-sub-title">{{$category->name}}</span>--}}
{{--                                                                            <ul class="dropdown-mega-sub-nav">--}}
{{--                                                                                @foreach($news as $new)--}}
{{--                                                                                    @if($new ->CategoryID == $category->id)--}}
{{--                                                                                <li>--}}
{{--                                                                                    <a class="dropdown-item" href="{{url("news/$new->id")}}" data-ajax-on-modal>--}}
{{--                                                                                        {{$new->title}}--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                                    @endif--}}
{{--                                                                                @endforeach--}}

{{--                                                                            </ul>--}}
{{--                                                                        </div>--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="{{url('team')}}">
                                                {{__('app.Team')}}
                                            </a>
                                        </li>

                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="{{url('contact')}}">
                                                {{__('app.contact')}}
                                            </a>
                                        </li>
{{--                                        <li class="btn btn-dark btn-lang">--}}
                                            <div class="order-1 order-lg-2" style="margin: auto">
{{--                                                <div class="header-nav-feature header-nav-features-social-icons d-inline-flex">--}}
{{--                                                    <ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean ml-0">--}}

                                                        @if(LaravelLocalization::getCurrentLocale()=='en')
                                                            <li class="btn btn-dark" style="text-align: center; ">
                                                                <a style="color: #fff !important; width: auto; min-height: 0" href="{{url('changeLang/ar')}}">العربية</a></li>
                                                        @else

                                                            <li class="btn btn-dark py-2" style="text-align: center;">
                                                                <a style="color: #fff !important; width: auto; min-height: 0" href="{{url('changeLang/en')}}">English</a></li>
                                                        @endif
{{--                                                    </ul>--}}
                                                </div>
{{--                                            </div>--}}
{{--                                        </li>--}}
                                    </ul>

                                </nav>

                            </div>

                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>



