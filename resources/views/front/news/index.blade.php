@extends('layouts.site')

@section('title')
    {{__('app.News')}}
@endsection
@section('content')
    <div id="team_page" role="main" class="main "  @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right" @else dir="ltr" @endif>
        <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{asset('front/img/news.jpg')}});">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <h1 class="text-10">{{__('app.News')}}</h1>
                        <span class="sub-title pt-2">{{__('app.Check out our Latest News')}}</span>
                    </div>
                </div>
            </div>
        </section>

        <div id="news" class="container py-4">

            <div class="row">
                <div class="col-lg-3">
                    <aside class="sidebar">
                        <h5 class="font-weight-semi-bold pt-4">{{__('app.Categories')}}</h5>
                        <ul class="nav nav-list flex-column mb-5">
                            @foreach($categories as $category)
                                @if (count($category->News))
                                    <li class="nav-item list">
                                        <a class="nav-link active" href="#">{{$category->name}}</a>
                                        <ul>
                                            @foreach($news as $new)
                                            @if($new ->CategoryID == $category->id)
                                            <li class="nav-item"><a href="{{url("news/$new->id")}}" data-ajax-on-modal>{{$new->title}}</a></li>
                                            @endif
                                            @endforeach

                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-9">
                    <div class="blog-posts">
                        @foreach($news as $new)
                        <article class="post post-medium">
                            <div class="row mb-3">
                                <div class="col-lg-5">
                                    <div class="post-image">
                                        <a href="blog-post.html">

                                            <img @if($new->image != null) src="{{asset("admin/{$new->image}")}}"  @else  src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}"@endif  class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0"  style="background: #778cbf;" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="post-content">
                                        <h2 class="font-weight-semibold pt-4 pt-lg-0 text-5 line-height-4 mb-2"><a href="{{url("news/$new->id")}}" data-ajax-on-modal>{{$new->title}}</a></h2>
                                        <p class="mb-0">{!!  \Illuminate\Support\Str::limit( $new->text, 350) !!}</p><br>
                                        <div class="post-meta">
                                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="{{url("news/$new->id")}}" data-ajax-on-modal class="btn btn-xs btn-light text-1 text-uppercase">{{__('app.Read More')}}</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </article>
                        @endforeach

                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
