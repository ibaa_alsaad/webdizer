@extends('layouts.site')

@section('title')
  {{__('app.contact')}}
@endsection
@section('content')
    <?php
    $variable =\App\Models\Variable::all();
    ?>
<div id="contact">
    <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
    <div id="googlemaps" class="google-map mt-0" style="height: 500px;"></div>

    <div class="container" @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right;
" @else dir="ltr" @endif>

        <div class="row py-4">
          <div class="col-lg-6">

                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
                    <h2 class="font-weight-bold text-8 mt-2 mb-5">{{__('app.contact')}}</h2>
                    <ul class="list list-icons list-icons-style-2 mt-2">
{{--                        <div class="row mt-5">--}}
{{--                            <div class="col-md-6">--}}
                                @if(LaravelLocalization::getCurrentLocale()=='ar')
                                    @if($variable[0]->getValueADR('address','ar') != null)
                                        <li>
                                            <i class="fas fa-map-marker-alt top-6"></i>
                                            <strong class="text-dark mb-3" style="margin: 2rem">{{__('app.address')}}: </strong> {{$variable[0]->getValueADR('address','ar')}}
                                        </li>
                                    @endif
                                @else
                                    @if($variable[0]->getValueADR('address','en') != null)
                                        <li>
                                            <i class="fas fa-map-marker-alt top-6"></i>
                                            <strong class="text-dark mb-3" style="margin: 2rem">{{__('app.address')}}: </strong> {{$variable[0]->getValueADR('address','en')}}
                                        </li>
                                    @endif
                                @endif
{{--                            </div>--}}
{{--                            <div class="col-md-6">--}}
                                @if($variable[0]->getValue('phone') != null)
                                    <li>
                                        <i class="fas fa-phone top-6"></i>
                                        <strong class="text-dark mb-3" style="margin: 2rem">{{__('app.phone')}}: </strong> <a target="_blank" href="tel:{{$variable[0]->getValue('phone')}}">{{$variable[0]->getValue('phone')}}</a>
                                    </li>
                                @endif
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mt-5">--}}
{{--                            <div class="col-md-6">--}}
                                @if($variable[0]->getValue('whatsup') != null)
                                    <li>
                                        <i class="fab fa-whatsapp top-6" style="font-weight: bold;"></i>
                                        <strong class="text-dark mb-3" style="margin: 2rem">{{__('app.WhatsApp')}}: </strong> <a target="_blank" href="tel:{{$variable[0]->getValue('whatsup')}}">{{$variable[0]->getValue('whatsup')}}</a>
                                    </li>
                                @endif
{{--                            </div>--}}
{{--                            <div class="col-md-6">--}}
                                @if($variable[0]->getValue('email') != null)
                                    <li>
                                        <i class="fas fa-envelope top-6"></i>
                                        <strong class="text-dark mb-3" style="margin: 2rem">{{__('app.Email')}}: </strong>  <a target="_blank" href="mailto:{{$variable[0]->getValue('email')}}">{{$variable[0]->getValue('email')}}</a>
                                    </li>
                                @endif
{{--                            </div>--}}
{{--                        </div>--}}

                    </ul>
                </div>

            </div>
            <div class="col-lg-6">

                <h2 class="font-weight-bold text-8 mt-2 mb-0">{{__('app.SendMessage')}}</h2>
                <p class="mb-4">{{__('app.contactForm')}}</p>

                <form id="contact-form" class="contact-form" action="{{route('send.email')}}" method="POST">
                    @csrf
                    @if(session()->has('message'))
                        <div class="contact-form-success alert alert-success d-none mt-4">
                            <strong> {{ session()->get('message') }} </strong>
                        </div>
                    @endif

                    <div class="contact-form-error alert alert-danger d-none mt-4">
                        <strong>Error!</strong> There was an error sending your message.
                        <span class="mail-error-message text-1 d-block"></span>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-lg-6">
                            <label class="mb-1 text-2">{{__('app.FullName')}}</label>
                            <input type="text" value="" data-msg-required="{{__('app.Please enter your Full Name.')}}" maxlength="100" class="form-control text-3 h-auto py-2" name="name" required>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="mb-1 text-2">{{__('app.Email')}}</label>
                            <input type="email" value="" data-msg-required="{{__('app.Please enter your email address.')}}" data-msg-email="{{__('app.Please enter a valid email address.')}}" maxlength="100" class="form-control text-3 h-auto py-2" name="email" required>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="mb-1 text-2">{{__('app.Subject')}}</label>
                            <input type="text" value="" data-msg-required="{{__('app.Please enter the subject.')}}" maxlength="100" class="form-control text-3 h-auto py-2" name="subject" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="mb-1 text-2">{{__('app.Message')}}</label>
                            <textarea maxlength="5000" data-msg-required="{{__('app.Please enter your message.')}}" rows="8" class="form-control text-3 h-auto py-2" name="msg" required></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <input type="submit" value="{{__('app.SendMessage')}}" class="btn btn-primary btn-modern" data-loading-text="Loading...">
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
@endsection
@section('script')
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>--}}
{{--<script>--}}
{{--    $(document).ready(function(){--}}

{{--        if($("#contact_form").length > 0)--}}
{{--        {--}}
{{--            $('#contact_form').validate({--}}
{{--                rules:{--}}
{{--                    name : {--}}
{{--                        required : true,--}}
{{--                        maxlength : 50--}}
{{--                    },--}}
{{--                    email : {--}}
{{--                        required : true,--}}
{{--                        maxlength : 50,--}}
{{--                        email : true--}}
{{--                    },--}}
{{--                    msg : {--}}
{{--                        required : true,--}}
{{--                        minlength : 50,--}}
{{--                        maxlength : 500--}}
{{--                    }--}}
{{--                },--}}
{{--                messages : {--}}
{{--                    name : {--}}
{{--                        required : 'Enter Name Detail',--}}
{{--                        maxlength : 'Name should not be more than 50 character'--}}
{{--                    },--}}
{{--                    email : {--}}
{{--                        required : 'Enter Email Detail',--}}
{{--                        email : 'Enter Valid Email Detail',--}}
{{--                        maxlength : 'Email should not be more than 50 character'--}}
{{--                    },--}}
{{--                    msg : {--}}
{{--                        required : 'Enter Message Detail',--}}
{{--                        minlength : 'Message Details must be minimum 50 character long',--}}
{{--                        maxlength : 'Message Details must be maximum 500 character long'--}}
{{--                    }--}}
{{--                }--}}
{{--            });--}}
{{--        }--}}

{{--    });--}}
{{--</script>--}}

@endsection
