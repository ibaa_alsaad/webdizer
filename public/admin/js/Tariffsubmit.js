/**
 * Created by Cesaer on 21/05/2017.
 */
$(document).ready(function(){
    ////////// saving data ////////////////////
    $('#save').click(function (e){

        var saveBtn = $(this);
        e.preventDefault();
        $('.help-block').html('');
        $('div').removeClass('has-error');
        var status = true;
        $('.req').each(function(){
            _this = $(this);
            if (_this.val() == "") {
                _this.closest("div").find('.help-block').html('<strong>يرجى تعبئة هذا الحقل بالبيانات</strong>');
                _this.closest("div").addClass('has-error');
                _this.focus();
                status = false;
            }
        });
        if (status == true) {
            //var post = $('.mytext');
            //post.textContent = postText;
            //post.innerHTML = post.innerHTML.replace(/\n/g, '<br>\n');
            // saveBtn.text('جاري الحفظ ....');
            var data = $(document).find('form').serialize();
            var url=$('#myForm').attr('action');

            $.post(url,data,function(res){
                if(res.success == true ){
               
                    new PNotify({
                        title: "تم",
                        text: res.msg,
                        type: 'success',
                        hideAfter:4000,
                        styling: 'bootstrap3',
                    });
                 

                }
                else{
                    new PNotify({
                        title:res.title,
                        text: res.msg,
                        type: 'error',
                        hideAfter:4000,
                        styling: 'bootstrap3'
                    });
                }
            },'json');
        }
    });
});
