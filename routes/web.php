<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserExperienceController;
use App\Http\Controllers\VariableController;
use App\Models\UserExperience;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


define('PAGINATION_COUNT',6);
Route::group(['prefix'=>'Admin' , 'Middleware'=>'auth' ],function (){
    Auth::routes();
    Route::get('/Password', [UserController::class,'showChangePasswordForm'])->name('Password');
    Route::post('changePassword', [UserController::class,'changePassword'])->name('changePassword');
    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index']);
    Route::resource('role',RoleController::class);
    Route::resource('users',UserController::class);
    Route::resource('service',ServiceController::class);
    Route::resource('project',ProjectController::class);
    Route::resource('slider',SliderController::class);
    Route::resource('news',NewsController::class);
    Route::resource('category',CategoryController::class);
    Route::resource('team',TeamController::class);
    Route::resource('userExperience',UserExperienceController::class);
    Route::resource('partners',PartnerController::class);
    Route::get('about',[App\Http\Controllers\VariableController::class,'getAbout']);
    Route::get('getAbout',[App\Http\Controllers\VariableController::class,'create']);
    Route::post('editAbout',[App\Http\Controllers\VariableController::class,'setAbout']);
    Route::get('contact_info',[App\Http\Controllers\VariableController::class,'getInfo']);
    Route::post('edit_contact_info',[App\Http\Controllers\VariableController::class,'setInfo']);
    Route::get('getMap',[App\Http\Controllers\VariableController::class,'getMap']);
    Route::post('setMap',[App\Http\Controllers\VariableController::class,'setMap']);


});
//route for multi languages

Route::get('changeLang/{lang}',function ($lang){
    App::setLocale($lang);
    Session::put('locale', $lang);
    \LaravelLocalization::setLocale($lang);
    $url = \LaravelLocalization::getLocalizedURL(App::getLocale(), \URL::previous());
    return Redirect::to($url);
});

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){

        Route::get('/', [App\Http\Controllers\HomeController::class, 'home']);
        Route::post('/search', [App\Http\Controllers\HomeController::class, 'search'])->name('search');
        Route::get('/search', [App\Http\Controllers\HomeController::class, 'index_search'])->name('index_search');
        Route::get('/team', [App\Http\Controllers\TeamController::class, 'show']);
//        Route::get('/contact', function (){
//            return view('front/contact');
//        });
        Route::get('/contact', [ContactUsController::class, 'create']);
        Route::post('/contact', [ContactUsController::class, 'sendEmail'])->name('send.email');

        Route::get('/service', [App\Http\Controllers\ServiceController::class, 'show'])->name('search_service');
        Route::get('/news', [App\Http\Controllers\NewsController::class, 'show']);
        Route::get('/news/{id}', [App\Http\Controllers\NewsController::class, 'show_detail'])->name('search_news');
        Route::get('/project', [App\Http\Controllers\ProjectController::class, 'show'])->name('search_project');
        Route::get('project/{id}', [App\Http\Controllers\ProjectController::class, 'show_detail']);
        Route::get('about',function(){
            if(\LaravelLocalization::getCurrentLocale()=='ar')
                $about=App\Models\Variable::where('name','about_ar')->first();
            else
                $about=App\Models\Variable::where('name','about_en')->first();
            return View::make('front/about-company')->with('about',$about);
        });
});

Route::get('/productsCat', [App\Http\Controllers\ProjectController::class, 'productsCat']);

