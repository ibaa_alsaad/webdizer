<?php


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;


function uploadImage($folder, $image)
{
    $image->store('/', $folder);
    $filename = $image->hashName();
    $path = 'images/' . $folder . '/' . $filename;
    return $path;
}


function deleteImage($folder, $image)
{
    $image_path = "admin/". '/' .$image;
        File::delete($image_path);
}

//function set_active( $route ) {
//    if( is_array( $route ) ){
//        return in_array(Request::path(), $route) ? 'active' : '';
//    }
//    return Request::path() == $route ? 'active' : '';
//}

