<?php

namespace App\Http\Controllers;

use App\Http\Requests\AllRequest;
use App\Models\Category;
use App\Models\CategoryTranslation;
use App\Models\News;
use App\Models\Project;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:التصنيفات|تصنيف المشاريع|عرض تصنيفات المشاريع|تصنيف الأخبار|عرض تصنيفات الأخبار|', ['only' => ['index']]);
        $this->middleware('permission:إضافة تصنيف مشروع|إضافة تصنيف خبر', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل تصنيف مشروع|تعديل تصنيف خبر', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف تصنيف مشروع|حذف تصنيف خبر', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //type=1 for project
        //type=2 for news
        $categories = Category::where('type', $request->type)->paginate(PAGINATION_COUNT);
        return view('admin.category.'.$request->type.'.index' , compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.category.'.$request->type.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validation =  $this->validate( $request, [
                'name:ar' => 'required',
                'name:en' => 'required',
            ]);
            $categories = Category::create([
                'name:ar' => $request -> {'name:ar'},
                'name:en' => $request -> {'name:en' },
                'type' => $request->type
            ]);

            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex) {
            return redirect()->back()->with(['error' => 'حدث خطأ ما']);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,Category $category)
    {
        $category = Category::select()->find($category->id);
        if(!$category){
            return redirect()->back()->with(['error'=>'هذه الخدمة غير موجودة']);
        }
            return view('admin.category.'.$category->type.'.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $categories = Category::where('id', $id)->first();
            $validation =  $this->validate( $request, [
                'name:ar' => 'required',
                'name:en' => 'required',
            ]);
            $categories->update([
                'name:ar' => $request -> {'name:ar'},
                'name:en' => $request -> {'name:en' },
                'type' => $request->type
            ]);

            return redirect('/Admin/category?type='.$request->type)->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex) {
            return redirect()->back()->with(['error' => 'حدث خطأ ما']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $categories = Category::find($request->delet_id);
            $projects = Project::where( 'CategoryID' ,$request->delet_id)->get();
            $news = News::where( 'CategoryID' ,$request->delet_id)->get();
            if (!$categories) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            else{
                if($projects||$news){
                    $categories->Project()->update([

                        'CategoryID' => null
                    ]);
                    $categories->News()->update([

                        'CategoryID' => null
                    ]);
                } $categories->delete();
            }


            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}
