<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:الخدمات|عرض الخدمات', ['only' => ['index']]);
        $this->middleware('permission:إضافة خدمة جديدة', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل خدمة', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف خدمة', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::select()->paginate(PAGINATION_COUNT);
        return view('admin.service.index' , compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            $filePath = "";
            if ($request->has('image')) {

                $filePath = uploadImage('service', $request->image);
            }

            $service = Service::create([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'image' => $filePath
            ]);

            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $services = Service::get();
        for($i=0;$i<count($services);$i++)
        {
            if($i%2==0)
                $services[$i]['class']='left';
            else
                $services[$i]['class']='right';

        }
        return view('front.service.index' , compact('services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $service = Service::select()->find($service->id);
        if(!$service){
            return redirect()->route('Admin.service')->with(['error'=>'هذه الخدمة غير موجودة']);
        }
        return view('admin.service.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $service = Service::where('id', $id)->first();
            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'image'   => 'mimes:jpg,jpeg,png',
            ]);
            //save image
            if ($request->has('image')) {
                $Path = deleteImage('service', $service->image);
                $filePath = uploadImage('service', $request->image);
            }
            else
                $filePath = $service->image;

            $service->update([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'image'   => $filePath
            ]);

            return redirect('Admin/service')->with(['success'=>'تم التحديث بنجاح']);
        }
        catch(\Exception $e){
            return redirect()->back()->with("error", 'حدث خطأ اثناء التعديل');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $services = Service::find($request->delet_id);
            if (!$services) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $Path = deleteImage('service', $services->image);

            $services->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}
