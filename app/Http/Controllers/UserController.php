<?php
namespace App\Http\Controllers;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:المستخدمين|قائمة المستخدمين', ['only' => ['index']]);
        $this->middleware('permission:إضافة مستخدم جديد', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل مستخدم', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف مستخدم', ['only' => ['destroy']]);

    }
    public function index(){
        $data = User::select()->paginate(PAGINATION_COUNT);
        return view('admin.users.index',compact('data'));
    }

    public function create(){
        $roles = Role::all();
        return view('admin.users.create',compact('roles'));
    }

    public function store(UserRequest $request){

        if(!$request->has('status'))
            $request->request->add(['status'=>0]);
//        $this->validate($request, [
//            'name' => 'required',
//            'email' => 'required|email|unique:users,email',
//            'password' => 'required|string|min:6',
//            'role_name' => 'required',
//        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);

        $user->assignRole($request->input('role_name'));

        return redirect()->back()->with('success','تم الحفظ بنجاح');
    }

//    public function show($id){
//        $user = User::find($id);
//        return view('users.show',compact('user'));
//    }

    public function edit($id){
        $user = User::find($id);
        $roles = Role::all();
        return view('Admin.users.edit',compact('user','roles'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|string|min:6',
            'role_name' => 'required',
            ]);

        if(!$request->has('status'))
            $request->request->add(['status'=>0]);
        else
            $request->request->add(['status'=>1]);

          $input = $request->all();

        if(!empty($input['password']))
          {
              $input['password'] = Hash::make($input['password']);
          }
        else{$input = array_except($input,array('password'));
        }
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('role_name'));
        return redirect()->route('users.index')->with('success','تم النعديل بنجاح');
    }

    public function destroy( Request $request ){
        try {
            $users = User::find($request->delet_id);
            if (!$users) {
                return redirect('Admin/users')->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $users->delete();

            return redirect('Admin/users')->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect('Admin/users')->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }

    }
    public function showChangePasswordForm(){
        return view('auth.passwords.reset');
    }
    public function changePassword(Request $request)
    {

//        dd(Auth::user());
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","كلمة المرور الحالية غير صحيحة ارجو المحاولة مرة أخرى");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","كلمة المرور الجديدة يجب أن تكون مغايرة للكلمة القديمة");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","تم تغيير كلمة المرور بنجاح");
        //return view('auth.changepassword');
    }
}
