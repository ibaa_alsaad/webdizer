<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SliderController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:سلايدر الصفحة الرئيسية| عرض Slider', ['only' => ['index']]);
        $this->middleware('permission:إضافة Slider جديد', ['only' => ['create','store']]);
        $this->middleware('permission: تعديل Slider', ['only' => ['edit','update']]);
        $this->middleware('permission: حذف Slider', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::select()->paginate(PAGINATION_COUNT);
        return view('admin.slider.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'image' => 'required|mimes:jpg,jpeg,png',
            ]);
            $filePath = "";
            if ($request->has('image')) {

                $filePath = uploadImage('slider', $request->image);
            }

            $sliders = Slider::create([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'image' => $filePath
            ]);
            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        $slider = Slider::find($slider->id);
        if(!$slider){
            return redirect()->route('Admin.slider')->with(['error'=>'هذا السلايدر غير موجود']);
        }
        return view('admin.slider.edit',compact('slider' ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $Id)
    {
        try {
            $slider = Slider::where('id', $Id)->first();
            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            //save image
            if ($request->has('image')) {
                $Path = deleteImage('slider', $slider->image);
                $filePath = uploadImage('slider', $request->image);
            }
            else
                $filePath = $slider->image;

            $slider->update([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'image' => $filePath
            ]);

            return redirect('Admin/slider')->with(['success'=>'تم التحديث بنجاح']);
        }
        catch(\Exception $e){
            return redirect()->back()->with("error", 'حدث خطأ اثناء التعديل');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $slider = Slider::find($request->delet_id);
            if (!$slider) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $Path = deleteImage('slider', $slider->image);
            $slider->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}
