<?php

namespace App\Http\Controllers;

use App\Http\Requests\AllRequest;
use App\Models\Category;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProjectController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:المشاريع| عرض المشاريع', ['only' => ['index']]);
        $this->middleware('permission: إضافة مشروع جديد', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل مشروع', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف مشروع', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::select()->paginate(PAGINATION_COUNT);
        return view('admin.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::get();
        return view('admin.project.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'CategoryID' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            $filePath = "";
            if ($request->has('image')) {

                $filePath = uploadImage('project', $request->image);
            }

            $projects = Project::create([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'CategoryID' => $request -> CategoryID,
                'image' => $filePath
            ]);
            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(){
        $projects=Project::paginate(4);
        $categories = Category::with('Project')->where('type', '1')->orderBy('id','DESC')->get();
        return view('front.project.index' , compact('projects','categories'));
    }
    public function productsCat(Request $request){
        $cat_id = $request->cat_id;

        $projects=Project::with('Category')->where('projects.CategoryID',$cat_id)->get();
        return view('front.project.projectPage' , compact('projects'));
    }
    public function show_detail($id){
        $project=Project::find($id);
        return view('front.project.detail' , compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $project = Project::find($project->id);
        $category = Category::get();
        if(!$project){
            return redirect()->route('Admin.project')->with(['error'=>'هذا المشروع غير موجود']);
        }
        return view('admin.project.edit',compact('project' , 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $Id)
    {
        try {
            $project = Project::where('id', $Id)->first();
            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'CategoryID' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            //save image
            if ($request->has('image')) {
                $Path = deleteImage('project', $project->image);
                $filePath = uploadImage('project', $request->image);
            }
            else
                $filePath = $project->image;

            $project->update([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'CategoryID' => $request -> CategoryID,
                'image' => $filePath
            ]);

            return redirect('Admin/project')->with(['success'=>'تم التحديث بنجاح']);
        }
        catch(\Exception $e){
            return redirect()->back()->with("error", 'حدث خطأ اثناء التعديل');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $projects = Project::find($request->delet_id);
            if (!$projects) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $Path = deleteImage('project', $projects->image);
            $projects->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}
