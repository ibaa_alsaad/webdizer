<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
class RoleController extends Controller{

    function __construct(){
        $this->middleware('permission:المستخدمين|صلاحيات المستخدمين', ['only' => ['index']]);
        $this->middleware('permission:إضافة مجموعة صلاحيات', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل صلاحيات', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف صلاحية', ['only' => ['destroy']]);
    }

    public function index(Request $request){
        $roles = Role::all();
        return view('admin.users.role',compact('roles'));
    }

    public function create(){
        $permission = Permission::get();
        return view('admin.users.createPermission',compact('permission'));
    }

    public function store(Request $request){
        try {
            $oldrole = Role::where('name', $request->name)->get();

            if (count($oldrole)) {
                return redirect()->back()->with('error','هذا الاسم مخزن سابقا');
            }
            else {
                $this->validate($request, [
                    'name' => 'required|unique:roles',
                    'permission' => 'required',
                ]);
                $permission_request=[];
                foreach ($request->permission as $key) {

                    $arr= explode('|', $key);
                    foreach ($arr as $p) {
                        array_push($permission_request,$p);
                    }

                }
                $role = Role::create(['name' => $request->input('name')]);
                $Permission=Permission::whereIn('id',$permission_request)->get();
                $role->syncPermissions($Permission);
                    return redirect()->back()->with('success', 'تمت الإضافة بنجاح');
            }

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }

    }

    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'permission' => 'required',
            ]);
            $permission_request=[];
            foreach ($request->permission as $key) {

                $arr= explode('|', $key);
                foreach ($arr as $p) {
                    array_push($permission_request,$p);
                }

            }
            $role = Role::find($id);
            $role->name = $request->input('name');
            $role->save();
            $Permission=Permission::whereIn('id',$permission_request)->get();
            $role->syncPermissions($Permission);
            return redirect()->back()->with('success', 'تمت التعديل بنجاح');

          } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
    }


    public function edit($id){

        $roles = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")
            ->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        return view('admin.users.editPermission',compact('roles','permission','rolePermissions'));
    }



    public function destroy(Request $request){
        try {
            $roles = Role::find($request->delet_id);
            if (!$roles) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $roles->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }

    }
}
