<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExperience extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name', 'text' ];
    protected $fillable=[];
    public function UserExperienceTranslation()
    {
        return $this->hasMany(UserExperienceTranslation::class,'user_experience_id');
    }
    public function scopeSelect($query)
    {
        return $query->orderBy('id','DESC');
    }
}

