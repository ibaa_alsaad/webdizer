<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variable extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [ 'value' ];
    protected $fillable=['name'];
    public function VariableTranslation()
    {
        return $this->hasMany(VariableTranslation::class,'variable_id');
    }
    public function getValueADR($name ,$lang = "en"){
        $var=Variable::where('name',$name)->first();
        if($var){
            if($lang == 'ar')
                return $var->{'value:ar'};
            return $var->{'value:en'};
        }
        return 'empty';
    }
    public function getValue($name){
        $var=Variable::where('name',$name)->first();
        if($var){
            return $var->value;
        }
        return 'empty';
    }
}
