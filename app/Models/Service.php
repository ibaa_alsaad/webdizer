<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['title', 'text'];
    protected $fillable=['image'];
    public function ServiceTranslation()
    {
        return $this->hasMany(ServiceTranslation::class,'service_id');
    }

    public function scopeSelect($query)
    {
        return $query->orderBy('id','DESC');
    }

}
