<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VariableTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable=['value'];
    public function Variable()
    {
        return $this->belongsTo(Variable::class,'variable_id');
    }
}
