<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model implements TranslatableContract
{
    use HasFactory,  Translatable;

    public $translatedAttributes = ['title', 'text'];
    protected $fillable = ['image'];
    public function SliderTranslation()
    {
        return $this->hasMany(SliderTranslation::class, 'slider_id');
    }

    public function scopeSelect($query)
    {
        return $query->orderBy('id', 'DESC');
    }
}
