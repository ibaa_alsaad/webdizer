<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class ProjectTranslation extends Model implements Searchable
{
    use HasFactory;
//    public $timestamps = false;
    protected $fillable=['title','text'];

    public function getSearchResult(): SearchResult
    {
        $url = route('search_project', $this->id);
        $text = $this->text;

        return new SearchResult(
            $this,
            $this->title,
//            $this->text,
//            $this->created_at,
            $url
        );
    }

    public function Project()
    {
        return $this->belongsTo(Project::class,'project_id');
    }
}
