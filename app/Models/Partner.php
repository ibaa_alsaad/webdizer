<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;
    protected $fillable=['image','link'];

    public function scopeSelect($query)
    {
        return $query->orderBy('id','DESC');
    }
}
