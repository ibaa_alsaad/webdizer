<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExperienceTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable=['name','text'];
    public function UserExperience()
    {
        return $this->belongsTo(UserExperience::class,'user_experience_id');
    }
}
